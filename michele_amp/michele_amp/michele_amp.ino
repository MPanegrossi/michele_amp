#include <ESP8266WiFi.h>          //https://github.com/esp8266/Arduino

//needed for library
#include <DNSServer.h>
#include <ESP8266WebServer.h>
#include <WiFiManager.h>         //https://github.com/tzapu/WiFiManager

#define speaker1LPin 13
#define speaker1RPin 12
#define speaker2LPin 14
#define speaker2RPin 16

#define powerPin 5

int speaker1LPinState = 0;
int speaker2LPinState = 0;
int powerPinState = 0;


//CREATE A WEBSERVER
// Create an instance of the server
// specify the port to listen on as an argument
WiFiServer server(80);

IPAddress MyIp;

void setup() {
  
  Serial.begin(115200);
  while(!Serial)
  {
    //JUST WAIT
  }


  pinMode(speaker1LPin,OUTPUT);
  pinMode(speaker1RPin,OUTPUT);
  pinMode(speaker2LPin,OUTPUT);
  pinMode(speaker2RPin,OUTPUT);
  pinMode(powerPin,OUTPUT);


  digitalWrite(speaker1LPin,LOW);
  digitalWrite(speaker1RPin,LOW);
  digitalWrite(speaker2LPin,LOW);
  digitalWrite(speaker2RPin,LOW);
  digitalWrite(powerPin,LOW);

  
  //WiFiManager
  //Local intialization. Once its business is done, there is no need to keep it around
  WiFiManager wifiManager;
  //reset saved settings
  //wifiManager.resetSettings();
  
  //set custom ip for portal
  wifiManager.setSTAStaticIPConfig(IPAddress(192,168,1,100), IPAddress(192,168,1,1), IPAddress(255,255,255,0));

  //fetches ssid and pass from eeprom and tries to connect
  //if it does not connect it starts an access point with the specified name
  //here  "AutoConnectAP"
  //and goes into a blocking loop awaiting configuration
  //wifiManager.autoConnect("AutoConnectAP");
  //or use this for auto generated name ESP + ChipID
  wifiManager.autoConnect();

  
  //if you get here you have connected to the WiFi
  Serial.println("connected...yeey :)");

  // Start the server
  server.begin();
  Serial.println("Server started");
    

   MyIp = WiFi.localIP();
   Serial.println(MyIp);
}


//SOME USEFUL FUNCTIONS

String trimReq(String r)
{
           r.remove(0,5);
          //Serial.println(req);
          int start = r.indexOf("/");
          //Serial.print("index of / is:");
          //Serial.println(start);
          int request_length = r.substring(0,start).toInt();
          //Serial.print("length of cue is:");
         // Serial.println(cue_length);
          String request = r.substring(start+1,request_length+start+1);
          //Serial.print("Thecue is:");
         // Serial.println(cue);
                    
          return request;
}

void loop() {

    // Check if a client has connected
    WiFiClient client = server.available();

    if(client)
    {
      
      //Serial.println("new client");
      while(!client.available()){
        delay(1);
      }
        //Receive HTTP req string form Processing
        String req = client.readStringUntil('\r');
        //Serial.println(req);
        client.flush();

        // Match the request
        if (req.indexOf("/bedroom/0") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);
          
          digitalWrite(speaker1LPin,LOW);
          digitalWrite(speaker1RPin,LOW);
          speaker1LPinState = 0;

          Serial.println("Bedroom is OFF");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Bedroom is OFF\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }
        
        if (req.indexOf("/bedroom/1") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);
          
          digitalWrite(speaker1LPin,HIGH);
          digitalWrite(speaker1RPin,HIGH);
          speaker1LPinState = 1;

          Serial.println("Bedroom is ON");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Bedroom is ON\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
        }

        // Match the request
        if (req.indexOf("/kitchen/0") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);
          
          digitalWrite(speaker2LPin,LOW);
          digitalWrite(speaker2RPin,LOW);
          speaker2LPinState = 0;

          Serial.println("Kitchen is OFF");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Kitchen is OFF\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }

                // Match the request
        if (req.indexOf("/kitchen/1") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);
          
          digitalWrite(speaker2LPin,HIGH);
          digitalWrite(speaker2RPin,HIGH);
          speaker2LPinState = 1;


          Serial.println("Kitchen is ON");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Kitchen is ON\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }

        // Match the request
        if (req.indexOf("/both/0") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);

          digitalWrite(speaker1LPin,LOW);
          digitalWrite(speaker1RPin,LOW);
          digitalWrite(speaker2LPin,LOW);
          digitalWrite(speaker2RPin,LOW);
          speaker2LPinState = 0;
          speaker1LPinState = 0;

          Serial.println("Both are OFF");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Both are OFF\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }

                        // Match the request
        if (req.indexOf("/both/1") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);

          digitalWrite(speaker1LPin,HIGH);
          digitalWrite(speaker1RPin,HIGH);
          digitalWrite(speaker2LPin,HIGH);
          digitalWrite(speaker2RPin,HIGH);
          speaker2LPinState = 1;
          speaker1LPinState = 1;

          Serial.println("Both are ON");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Both are ON\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }

        if (req.indexOf("/amp/0") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);
          
          digitalWrite(powerPin,LOW);
          powerPinState = 0;

          Serial.println("AmpOFF");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Amp is OFF\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }

        if (req.indexOf("/amp/1") != -1)
        {
          //Format the response
          //char out[32];
          //trimReq(req).toCharArray(out,32);
          
          digitalWrite(powerPin,HIGH);
          powerPinState = 1;

          Serial.println("AmpON");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":\"Amp is ON\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }

                
        if (req.indexOf("/state") != -1)
        {
          
          Serial.println("StateRequest");
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"success\",\"message\":{\"amp\":" + String(powerPinState) + ",\"bedroom\":" + String(speaker1LPinState) + ",\"kitchen\":" + String(speaker2LPinState) + "}}";
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }

        if (req.indexOf("/amp/1") == -1 && req.indexOf("/amp/0") == -1 && req.indexOf("/both/1") == -1 && req.indexOf("/both/0") == -1 && req.indexOf("/bedroom/1") == -1 && req.indexOf("/bedroom/0") == -1 && req.indexOf("/kitchen/1") == -1 && req.indexOf("/kitchen/0") == -1 && req.indexOf("/state") == -1)
        {
          
          //Send somthing back to Processing
          String s = "HTTP/1.1 200 OK\r\nContent-Type: application/json\r\n\r\n{\"status\":\"failure\",\"message\":\"Bad formatting\"}";
         /* s += (val)?"high":"low";
          s += "</html>\n";*/
          
          // Send the response to the client
          client.print(s);
          delay(1);
         
        }


   
    }
    
}
